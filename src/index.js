import "./sass/index.scss";

import feather from "feather-icons";

feather.replace();


// main header tabs animation
const els = document.querySelectorAll(".header .header__tabs .tab");
els.forEach(el => 
    el.addEventListener('click', (e) => {
        if(e.currentTarget.classList.contains('tab--active'))
            return;
        els.forEach(el => el.classList.remove('tab--active'));
        e.currentTarget.classList.add('tab--active')
    })
);

// chat header tabs animation
const _els = document.querySelectorAll(".chat .chat__msg__header .tab");
_els.forEach(el => 
    el.addEventListener('click', (e) => {
        if(e.currentTarget.classList.contains('tab--active'))
            return;
        _els.forEach(el => el.classList.remove('tab--active'));
        e.currentTarget.classList.add('tab--active')
    })
);

// users animation
const users = document.querySelectorAll('.chat .chat__with__users .user');
console.log(users);
users.forEach(user => user.addEventListener('click', (e) => {
    if(e.currentTarget.classList.contains('active'))
        return;
    users.forEach(el => el.classList.remove('active'));
    e.currentTarget.classList.add('active');
}));

// toggle bar animation
const bar = document.getElementById('add-bar'); 
const items = bar.querySelectorAll('.add-bar__item');
items.forEach(item => item.addEventListener('click', (e) => {
    if(e.currentTarget.classList.contains('active'))
        return;
    items.forEach(el => el.classList.remove('active'));
    e.currentTarget.classList.add('active')
}));

// toggle btn animation
document.querySelector('#toggle .btn').addEventListener('click', (e) => {
    e.currentTarget.classList.toggle('active');
    document.getElementById('add-bar').classList.toggle('hide');
});


//  watch for sass changes
const watch = require('glob-watcher');
const watcher = watch(['./**/*.scss']);

const parcel = getParcelInstance(); 
watcher.on('change', function(path, stat) {
  console.log('changed', path, stat);
  parcel.fullReload();
});
 
watcher.on('add', function(path, stat) {
  console.log('added', path, stat);
  parcel.fullReload();
});


